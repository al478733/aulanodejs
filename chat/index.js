const express = require('express');
const http = require('http');
const socketIO = require('socket.io');
const os = require('os');

const app = express();
const server = http.createServer(app);
const io = socketIO(server);

const getLocalIpAddress = () => {
  const interfaces = os.networkInterfaces();
  for (const key in interfaces) {
    for (const iface of interfaces[key]) {
      if (!iface.internal && iface.family === 'IPv4') {
        return iface.address;
      }
    }
  }
  return '127.0.0.1'; // fallback to localhost
};

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', (socket) => {
  console.log('Usuário conectado');

  socket.on('disconnect', () => {
    console.log('Usuário desconectado');
  });

  socket.on('chat message', (msg) => {
    console.log('Mensagem recebida: ' + msg);
    io.emit('chat message', msg); // Envia a mensagem para todos os clientes conectados
  });
});

server.listen(3000, () => {
  const ipAddress = getLocalIpAddress();
  console.log(`Servidor rodando em http://${ipAddress}:3000/`);
});